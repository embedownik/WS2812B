# WS28B12 for STM32 HAL library.

This is very basic driver - with options to set given led to selected color, refresh all leds and some additional test animations.

Periph requirements:
	- SPI (only mosi pin used)
	- DMA channel
	
No double buffering etc - just minimal implementation (because this is used on small STM32F030F4P6 project).

## How to use this library

- add to your project file "board.h" with #define for leds number, example:

```
#define WS28B12_LEDS_NUMBER 24
```

- configure with CubeMX SPI and DMA for WS28B12 communication; Requirements:

- output frequency for SPI -> 3MHz, so in CubeMx you should see Baudrate 3.0 MBits/s

![obr](./docs/026_001.png)

- enabled DMA channel for SPI - module transmit data with "HAL_SPI_Transmit_DMA", for example:

![obr](./docs/026_002.png)

- in code -> init module with handler to this instance, for example:

```c
WS28B12_Init(&hspi1);
```

where "hspi1" is handler from CubeMx.

Next during runtime - set some pixels colors and refresh all:

```c
static const color_HSV_s someColor =
{
    .h = HSV_H_AQUA,
    .s = 100,
    .v = 100,
};

WS28B12_SetPixel_HSV(0, someColor);
WS28B12_SetPixel_HSV(2, someColor);
WS28B12_SetPixel_HSV(3, someColor);

WS28B12_RefreshScreen();
```