/*
 * WS28B12.h
 *
 *      Author: Embedownik
 */

#ifndef WS28B12_WS28B12_H_
#define WS28B12_WS28B12_H_

#include <stdint.h>

#include <board.h>

/* Basic structs for colors. */
typedef struct
{
    uint8_t g;
    uint8_t r;
    uint8_t b;
} color_RGB_s;

typedef struct
{
    uint8_t h;
    uint8_t s;
    uint8_t v;
} color_HSV_s;

/* Some color defines in HSV color space */
#define HSV_H_RED     (  0U)
#define HSV_H_YELLOW  ( 42U)
#define HSV_H_GREEN   ( 85U)
#define HSV_H_AQUA    (128U)
#define HSV_H_BLUE    (171U)
#define HSV_H_MAGENTA (214U)

/**********************************************************************************************************************/
/**                                                 BASIC INTERFACE                                                  **/
/**********************************************************************************************************************/

/**
 * Init this library with handler for SPI.
 * This way SPI can be configured by CubeMX/CubeIDE and there will be no "extern".
 *
 * Info about SPI configuration in library readme.
 */
void WS28B12_Init(SPI_HandleTypeDef* spiHandler);

/**
 * Set selected pixel color.
 *
 * Requires display refreshing.
 */
void WS28B12_SetPixel_RGB(uint8_t pixelNumber, color_RGB_s color);

/**
 * Set selected pixel color.
 *
 * Requires display refreshing.
 */
void WS28B12_SetPixel_HSV(uint8_t pixelNumber, color_HSV_s color);

/**
 * Clears internal ledBuffer - all leds are black.
 *
 * Requires display refreshing.
 */
void WS28B12_ClearBuffer(void);

/**
 * Refresh screen with actual internal buffer content.
 */
void WS28B12_RefreshScreen(void);

/**
 * Set all leds to selected color.
 *
 * Requires display refreshing.
 */
void WS28B12_SetAllLeds_RGB(const color_RGB_s* color);

/**
 * Set all leds to selected color.
 *
 * Requires display refreshing.
 */
void WS28B12_SetAllLeds_HSV(const color_HSV_s* colorHSV);

/**********************************************************************************************************************/
/**                                                     HELPERS                                                      **/
/**********************************************************************************************************************/

/**
 * Convert HSV color to rgb color.
 * Function from site: http://mirekk36.blogspot.com/2015/06/ws2812-magic-led-przestrzen-barw-hsv.html
 *
 * H:   0-255, 0=Red, 42=Yellow, 85=Green, 128=Aqua, 171=Blue, 214=Magenta
 * S:   0-255, 0 oznacza tylko biala barwe, 255 pelne nasycenie koloru
 * V:   0-255, 0=brak swiatla, 255=maksymalna jasnosc
 */
void WS28B12_HSV2RGB(const color_HSV_s* hsv, color_RGB_s* rgb);

/**********************************************************************************************************************/
/**                                                 TESTS/ANIMATIONS                                                 **/
/**                                Tests are "single" actions, animations are mostly "continous".                    **/
/** Warning-by default animations are "blocking". If you are using RTOS change delay_ms implementation to OS version **/
/**********************************************************************************************************************/

/**
 * Set all leds to selected color and refresh display.
 */
void WS28B12_TEST_SingleColorRGB(color_RGB_s* kolor_rgb);

/**
 * Start test animaion - rainbow.
 *
 * WARNING - BLOCKING!!!
 *
 * @param saturation start saturation
 * @param value      start value
 * @param delay      delay between animation steps
 * @param color_step single step increment value
 */
void WS28B12_TEST_Animation_Rainbow(uint8_t saturation, uint8_t value, uint16_t delay, uint16_t color_step);

#endif
