#include <string.h>

#include <WS28B12.h>

/* Size of data buffer for DMA transmission */
#define WS28B12_BUFF_SIZE (((WS28B12_LEDS_NUMBER + 1U) * 9U))

/*
 * Internal buffer with all pixels colors values
 * Right now only single buffering is supported.
 */
static color_RGB_s ws28b12_buff[WS28B12_LEDS_NUMBER];

/* Buffer with calculated data to send via SPI and DMA */
static uint8_t ws28b12_dma_buff[WS28B12_BUFF_SIZE];

/* internal pointer for HAL_SPI handler - from Init function */
static SPI_HandleTypeDef* ws28b12_spiHandler;

/* look-up table for gamma correction for led brightness */
static const uint8_t dim_curve[] =
{
    0,   1,   1,   2,   2,   2,   2,   2,   2,   3,   3,   3,   3,   3,   3,   3,
    3,   3,   3,   3,   3,   3,   3,   4,   4,   4,   4,   4,   4,   4,   4,   4,
    4,   4,   4,   5,   5,   5,   5,   5,   5,   5,   5,   5,   5,   6,   6,   6,
    6,   6,   6,   6,   6,   7,   7,   7,   7,   7,   7,   7,   8,   8,   8,   8,
    8,   8,   9,   9,   9,   9,   9,   9,   10,  10,  10,  10,  10,  11,  11,  11,
    11,  11,  12,  12,  12,  12,  12,  13,  13,  13,  13,  14,  14,  14,  14,  15,
    15,  15,  16,  16,  16,  16,  17,  17,  17,  18,  18,  18,  19,  19,  19,  20,
    20,  20,  21,  21,  22,  22,  22,  23,  23,  24,  24,  25,  25,  25,  26,  26,
    27,  27,  28,  28,  29,  29,  30,  30,  31,  32,  32,  33,  33,  34,  35,  35,
    36,  36,  37,  38,  38,  39,  40,  40,  41,  42,  43,  43,  44,  45,  46,  47,
    48,  48,  49,  50,  51,  52,  53,  54,  55,  56,  57,  58,  59,  60,  61,  62,
    63,  64,  65,  66,  68,  69,  70,  71,  73,  74,  75,  76,  78,  79,  81,  82,
    83,  85,  86,  88,  90,  91,  93,  94,  96,  98,  99,  101, 103, 105, 107, 109,
    110, 112, 114, 116, 118, 121, 123, 125, 127, 129, 132, 134, 136, 139, 141, 144,
    146, 149, 151, 154, 157, 159, 162, 165, 168, 171, 174, 177, 180, 183, 186, 190,
    193, 196, 200, 203, 207, 211, 214, 218, 222, 226, 230, 234, 238, 242, 248, 255,
};

static void clearDMABuff(void)
{
    memset(ws28b12_dma_buff, 0U, WS28B12_BUFF_SIZE);
}

void WS28B12_ClearBuffer(void)
{
    memset(ws28b12_buff, 0U, sizeof(ws28b12_buff[0]) * WS28B12_LEDS_NUMBER);
}

/*
 * Added optimalization because this function is quite long (loops in loop)
 * and is tested that there is no problem to debug here.
 */
static __attribute__((optimize("O2"))) void convertBufferForDMA(void)
{
    uint16_t DMAbuffIndex = 0;
    uint8_t bitMask = 0b10000000;

    clearDMABuff();

    /* Loop over all leds */
    for(uint16_t ledNumber = 0; ledNumber < WS28B12_LEDS_NUMBER; ledNumber++)
    {
        for(uint8_t color = 0; color < 3U; color++)
        {
            uint8_t actualColor;

            switch(color)
            {
                case 0:
                {
                    actualColor = ws28b12_buff[ledNumber].g;
                    break;
                }
                case 1:
                {
                    actualColor = ws28b12_buff[ledNumber].r;
                    break;
                }
                case 2:
                {
                    actualColor = ws28b12_buff[ledNumber].b;
                    break;
                }
                default:
                {
                    break;
                }
            }

            /* Loop over all bits in selected color */
            for(uint8_t y = 128; y != 0; y = y >> 1)
            {
                ws28b12_dma_buff[DMAbuffIndex] |= bitMask;

                bitMask = bitMask >> 1;

                if(!bitMask)
                {
                    bitMask = 0b10000000;
                    DMAbuffIndex++;
                }

                if(actualColor & y)
                {
                    ws28b12_dma_buff[DMAbuffIndex] |= bitMask;
                }

                bitMask = bitMask >> 1;

                if(!bitMask)
                {
                    bitMask = 0b10000000;
                    DMAbuffIndex++;
                }

                bitMask = bitMask >> 1;

                if(!bitMask)
                {
                    bitMask = 0b10000000;
                    DMAbuffIndex++;
                }
            }
        }
    }
}

static void DMA_sendDataStart(void)
{
    /* use earlier configured SPI instance - accesss via ST HAL library */
    HAL_SPI_Transmit_DMA(ws28b12_spiHandler, ws28b12_dma_buff, WS28B12_BUFF_SIZE);
}

void WS28B12_Init(SPI_HandleTypeDef* spiHandler)
{
    ws28b12_spiHandler = spiHandler;
}

void WS28B12_SetPixel_RGB(uint8_t pixelNumber, color_RGB_s color)
{
    if(pixelNumber < WS28B12_LEDS_NUMBER)
    {
        ws28b12_buff[pixelNumber] = color;
    }
}

void WS28B12_SetPixel_HSV(uint8_t pixelNumber, color_HSV_s color)
{
    if(pixelNumber < WS28B12_LEDS_NUMBER)
    {
        color_RGB_s colorRGB;

        WS28B12_HSV2RGB(&color, &colorRGB);

        ws28b12_buff[pixelNumber] = colorRGB;
    }
}

void WS28B12_RefreshScreen(void)
{
    convertBufferForDMA();

    DMA_sendDataStart();
}

void WS28B12_SetAllLeds_RGB(const color_RGB_s* color)
{
    for(uint16_t x = 0; x < WS28B12_LEDS_NUMBER; x++)
    {
        ws28b12_buff[x] = *color;
    }
}

void WS28B12_SetAllLeds_HSV(const color_HSV_s *colorHSV)
{
    color_RGB_s rgb;

    WS28B12_HSV2RGB(colorHSV, &rgb);

    WS28B12_SetAllLeds_RGB(&rgb);
}

void __attribute__((optimize("O2"))) WS28B12_HSV2RGB(const color_HSV_s* hsv, color_RGB_s* rgb)
{
    color_HSV_s hsvColor = *hsv;

    hsvColor.v = dim_curve[hsvColor.v];
    hsvColor.s = 255U - dim_curve[255U - hsvColor.s];

    uint16_t region, remainder, p, q, t;

    if(hsvColor.s == 0U)
    {
        rgb->r = hsvColor.v;
        rgb->g = hsvColor.v;
        rgb->b = hsvColor.v;
    }
    else
    {
        region = ((uint16_t)hsvColor.h * 6U) / 256U;
        remainder = ((uint16_t)hsvColor.h * 6U) % 256U;

        p = (hsvColor.v * (255U - hsvColor.s)) / 256U;
        q = (hsvColor.v * (255U - (hsvColor.s * remainder) / 256U)) / 256U;
        t = (hsvColor.v * (255U - (hsvColor.s * (255U - remainder)) / 256U)) / 256U;

        switch(region) {
            case 0:
            {
                rgb->r = hsvColor.v;
                rgb->g = t;
                rgb->b = p;
                break;
            }
            case 1:
            {
                rgb->r = q;
                rgb->g = hsvColor.v;
                rgb->b = p;
                break;
            }
            case 2:
            {
                rgb->r = p;
                rgb->g = hsvColor.v;
                rgb->b = t;
                break;
            }
            case 3:
            {
                rgb->r = p;
                rgb->g = q;
                rgb->b = hsvColor.v;
                break;
            }
            case 4:
            {
                rgb->r = t;
                rgb->g = p;
                rgb->b = hsvColor.v;
                break;
            }
            case 5:
            {
                rgb->r = hsvColor.v;
                rgb->g = p;
                rgb->b = q;
                break;
            }
            default:
            {
                rgb->r = hsvColor.v;
                rgb->g = p;
                rgb->b = q;
                break;
            }
        }
    }
}
