#include <WS28B12.h>

static void _delay_ms(uint16_t time)
{
	HAL_Delay(time);
}

void WS28B12_TEST_SingleColorRGB(color_RGB_s* kolor_rgb)
{
	WS28B12_SetAllLeds_RGB(kolor_rgb);
	WS28B12_RefreshScreen();
}

void WS28B12_TEST_Animation_Rainbow(uint8_t saturation, uint8_t value, uint16_t delay, uint16_t color_step)
{
	uint8_t h_ofFirstLed = 0;

	for(;;)
	{
		uint8_t h_actual = h_ofFirstLed;

		for(uint8_t i = 0; i < WS28B12_LEDS_NUMBER; i++)
		{
			color_HSV_s tempColor =
			{
			    .s = saturation,
				.v = value,
				.h = h_actual,
			};

			color_RGB_s toSet;

			WS28B12_HSV2RGB(&tempColor, &toSet);

			WS28B12_SetPixel_RGB(i, toSet);

			h_actual += color_step;
		}

		WS28B12_RefreshScreen();

		h_ofFirstLed++;

		_delay_ms(delay);
	}
}
